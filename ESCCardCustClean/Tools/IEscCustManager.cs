﻿using System.Collections.Generic;
using ESCCardCustClean.Models;
using ShopifySharp;

namespace ESCCardCustClean.Tools
{
    public interface IEscCustManager
    {
        IEnumerable<EscCustObj> CusttoEscList(IEnumerable<ShopifyCustomer> Custs);

    }
}
