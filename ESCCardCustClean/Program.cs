﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using ShopifySharp;
using ShopifySharp.Filters;
using ESCCardCustClean.Models;
using ESCCardCustClean.Tools;
using Newtonsoft.Json;

namespace ESCCardCustClean
{
    class Program
    {
        public static IConfigurationRoot config { get; set; }

        static EscCustManager _EscCustManager = new EscCustManager();

        static int cnt = 0;

        static async void LoopCustomers()
        {
            var currentPage = 1;

            bool complete = false;
            var service = new ShopifyCustomerService(config["Shopify:CustomerWebsite"], config["Shopify:CustomerPassword"]);
            var customer = new EscCustObj();

            while (!complete)
            {
                System.Threading.Thread.Sleep(1000);

                var customers = service.ListAsync(new ShopifyListFilter() { Page = currentPage, Limit = 250 }).Result.ToList();

                foreach (ShopifyCustomer cust in customers)
                {

                    System.Threading.Thread.Sleep(1000);

                    var Tags = cust.Tags.Split(',');
                    var newTag = "";

                    if (cust.Tags != "")
                    {
                        foreach (var t in Tags)
                        {
                            if (t.Contains("ESC$"))
                            {
                                cnt = cnt + 1;
                                Console.WriteLine("Removed tag '" + t + "' for customer: " + cust.FirstName + " " + cust.LastName);
                            }
                            else
                            {
                                if (newTag != "")
                                {
                                    newTag = newTag + "," + t;
                                }
                                else
                                {
                                    newTag = t;
                                }
                                continue;
                            }
                        }

                        Console.WriteLine("customer: " + cust.FirstName + " " + cust.LastName + ", Tag: " + newTag);
                        var UpdatedCustomer = _EscCustManager.UpdateCust(cust, newTag.Replace(" ", ""));
                        System.Threading.Thread.Sleep(1000);
                        UpdatedCustomer = await service.UpdateAsync(UpdatedCustomer);
                    }
                }

                complete = !customers.Any();
                currentPage = currentPage + 1;
            }

            Console.WriteLine(Environment.NewLine + "******** Completed ********");
            Console.WriteLine("Total ESC$ tags were removed - " + cnt.ToString());
            Console.WriteLine("********** End ***********");
        }


        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            config = builder.Build();
            Console.WriteLine($"Website = {config["Shopify:CustomerWebsite"]}");
            Console.WriteLine($"Password = {config["Shopify:CustomerPassword"]}");
            LoopCustomers();

            Console.ReadLine();
        }
    }
}
